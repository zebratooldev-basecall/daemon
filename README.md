daemon
======

A simple daemon program for control zebra's task in cluster

Usage
-----

此daemon 程序使用crontab 来管理任务，使用方法是 crontab task.cron, crontab 的使用参考man手册，这里的task.cron是说每隔十分钟
运行一遍其中的任务，即调用daemon.sh, daemon.sh调用的是 daemon.py, 这个就是daemon程序的主程序

    $cat task.cron
    MAILTO=""
    */10 * * * * source ~/.bashrc &&  . $HOME/.profile && sh /share/Zebra/06.Script/daemon/daemon.sh >> /share/Zebra/06.Script/daemon/log.txt 2>&1

    $cat daemon.sh
    #!/bin/sh
    
    /rnd/home/liyuxiang/software/bin/python /share/Zebra/06.Script/daemon/daemon.py
    
流程说明
--------

当某台测序机器有新的run下机之后，会生成一个类似X1001_L01.txt的文件，放在/share/Zebra/05.SeqData/Info/Upload/ 目录，其中
包含了这个lane的基本信息，如机器号，slide，lane号，reference等，而真正的fastq文件都放在固定目录下，通过这个txt文件即可
找到。

我们的daemon程序每十分钟扫描一遍这个Upload目录，发现有新的文件，而且符合要mapping的条件，则会根据对应的lane 来投递任务，
并将此txt文件移动到同级的Archived 目录做备份，然后将获取的信息以json的数据格式放在/share/Zebra/06.Script/daemon/config/
task.json中

同样的，在每十分钟扫描有没有新的数据上传的同时，会使用drat命令来判断目前task.json中所有的任务状态：

    Running，任务正在跑，则什么也不做
    Done， 任务完成，发送邮件
    Error， 任务出错，将任务停掉，发送邮件给管理员(任务恢复之后需要将task.json中的任务状态改成正常)
  
  
文件解释
========

监控任务
--------
      
      task.cron  跑监控任务的cron 文件
      daemon.sh  daemon.py  主程序
      manageConfig.py  用来读取和写入config配置文件的模块
      manageTask.py   用来查询删除任务状态的模块
      removeError.sh  用来去除出错的任务状态，修复出错的任务之后使用 sh removeError.sh
      sendMail.py   专门用来发送邮件，其实发送邮件使用的是linux mutt命令，后面再详细谈
      simpleTask.py  如果你有一个单独的fastq来跑map，那么用这个脚本可以方便的准备数据，只需sh run.sh即可跑map
      config/common.cfg  主程序的配置文件，包括一堆路径和程序版本等
      config/input.json  一个备份的任务参数文件
      config/mailOwnerList.json  存储每台测序机器的机主邮件列表
                    注意的是：Zebra00 用来测试，实际不存在这台机器，而且如果发现一个不存在列表里的机器号，默认只发送给zebra00
                              CC  抄送列表
                              EE  如果任务报错，发送给这个列表，一般写管理员邮箱
      config/mailTemplate.html  发送邮件的主体模板

修改权限
--------

如果有一个run晚上12点下机，而别人着急分析，但没有权限，你又不可能晚上12点来改权限，那么用它就对了。
它可以将一个没有下机的数据修改权限。实际和daemon一样的，每隔一段时间去扫描目录，如果找到了你所给的目录，则修改权限

    task1.cron  跑修改权限的cron 文件
    changePermission.sh  controlPermission.py  主程序
    config/permissionControl.cfg  配置文件
    config/permissionControl.json  参数文件，里面写的是需要修改的slide，lane

发送邮件
--------

这里发送邮件使用的是linux的mutt 命令，它可以方便的发送html 文本的邮件，并且可以以hmtl文件作为附件,示例如下：

    echo "<html><head> <meta content-type="text/html" charset="utf-8"> </head> <b><p>Hi</p></b></html>" | 
      /home/zhaofuxiang/lib/mutt/bin/mutt -e "set content_type=text/html" -s test zhaofuxiang@genomics.cn -a task.cron
  
注意一定要使用正确的mutt版本，目前CG集群的mutt 版本太旧，需要自己安装Mutt 1.5.21，或者直接用我个人目录下的 /home/zhaofuxiang/lib/mutt/bin/mutt





    
