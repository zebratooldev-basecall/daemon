import os, sys
import glob
import json
import shutil

class ManageConfig(object):
    ''' 
        Class ManageConfig if used for read info file uploaded from seq machine
        and read or write to json file as config file.
    '''

    def __init__(self, configFile=None, infoFileAddr=None, archivedAddr=None):
        self.configFile = configFile
        self.infoFileAddr = infoFileAddr
        if archivedAddr == None:
            self.archivedAddr = os.path.join(os.path.dirname(self.infoFileAddr), 'Archived')
        else:
            self.archivedAddr = archivedAddr

        self.infoCategory = ['machine', 'slide', 'lane', 'reference', 'seqType', 'uploadTime', 'transferStatus']
        self.infoLength = len(self.infoCategory)

        if not os.path.exists(self.configFile):
            self.__initConfiFile()

    def __initConfiFile(self):
        infoDict = self.searchInfo()
        self.writeConfigFile(infoDict)

    def searchInfo(self):
        infoDict = {}

        ### Travels all info files like slide_lane.txt in Info/Upload directory
        ### Then get the info, and move them to Info/Archived
        infoFileList = glob.glob(os.path.join(self.infoFileAddr, '*_*.txt'))
        for infoFile in infoFileList:
            info = self.readSeqInfoFile(infoFile)
            if len(info) == self.infoLength:
                flag = '%s_%s' % (info[1], info[2])
                infoDict[flag] = {}
                for i in xrange(self.infoLength):
                    infoDict[flag][self.infoCategory[i]] = info[i]

                # Add extra category for processing task
                infoDict[flag]['taskStatus'] = 'prepare'
                infoDict[flag]['taskID'] = '0'


            # Move the info file
            #shutil.move(infoFile, self.archivedAddr)

        return infoDict 
        
    def readSeqInfoFile(self, seqInfoFile): 
        with open(seqInfoFile, 'r') as fhIn:
            seqData = fhIn.readlines()[-1]
            return seqData.strip().split()

    def readConfigFile(self):
        jsonData = {}

        with open(self.configFile) as fhIn:
            data = fhIn.read()
            jsonData = json.loads(data)

        return jsonData

    def writeConfigFile(self, jsonData, skipError=False):
        ### If skiped error status, then add it
        if skipError:
            tmpData = {}
            for d in jsonData:
                if jsonData[d]['transferStatus'] != 'Failed':
                    tmpData[d] = jsonData[d]
            jsonData = tmpData

        with open(self.configFile, 'w') as fhOut:
            fhOut.write(json.dumps(jsonData, sort_keys=True, indent=4))
            fhOut.close()

        return jsonData

if __name__ == '__main__':
    configFile = sys.argv[1]
    infoFileAddr = sys.argv[2]
    mc = ManageConfig(configFile, infoFileAddr)
    jsonData = mc.readConfigFile()
    jsonData.update(mc.searchInfo())
    mc.writeConfigFile(jsonData)
        




