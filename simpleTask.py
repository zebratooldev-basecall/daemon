import sys, os
import json
import shutil

class SimpleTask(object):

    def __init__(self, dataDir, mapDir, slide, reference, seqType):
        self.config = {}
        self.config['dataDir'] = dataDir
        self.config['mapDir'] = mapDir
        self.slide = slide
        self.reference = reference
        self.seqType = seqType

        self.config['programDir'] = '/prod/stor-01/opa/analyses/00.workflow/0.8.0/opt70max'
        self.config['taskScript'] = 'cpasOnestep.py -c MapFromFastq --clone-state-file=$(pwd)/input.json -w $(pwd) --project F13ZOOYJSY1380 --priority=10'
        self.config['submitServer'] = 'drat'



    def constructDir(self):
        destDir = self.config['mapDir']
        rawDataDir = self.config['dataDir']


        ### 0. Remove the original dir
        if os.path.exists(destDir):
            shutil.rmtree(destDir)
        ### 1. Directory
        os.makedirs(destDir)
        fqDir = os.path.join(destDir, 'FASTQ')
        if not os.path.exists(fqDir):
            os.makedirs(fqDir)
        link = os.path.join(fqDir, '%s' % (self.slide))
        if not os.path.exists(link):
            os.popen(r'ln -s %s %s' % (rawDataDir, link ))

        ### 2. Json file
        jsonFile = os.path.join(destDir, 'input.json')
        template = os.path.join(os.path.dirname(sys.argv[0]), 'config', 'input.json')
        with open(template, 'r') as fhIn:
            d= fhIn.read()
            jsonData = json.loads(d)

        ref = 'Ecoli'
        if self.reference == 'Ecoli.fa':
            ref = 'Ecoli'
        elif self.reference == 'Human.fa':
            ref = 'hg19'
        else:
            ref = self.reference.split('.')[0]
        jsonData["fqmap"]["ref"] = ref
            
        if self.seqType == 'PE':
            jsonData["input"]["isSe"] = False
        else:
            jsonData["input"]["isSe"] = True

        jsonData["cpasstat"]["runFqstat"] = True

        fhOut = open(jsonFile, 'w')
        fhOut.write(json.dumps(jsonData, indent=4, sort_keys=True))

        ### 3. Copy run.sh
        shellFile = os.path.join(destDir, 'run.sh')
        with open(shellFile, 'w') as fhOut:
            taskName = self.slide
            resultString = 'setcgi %s\n%s create %s %s' % (self.config['programDir'], self.config['submitServer'], taskName, self.config['taskScript'])
            fhOut.write(resultString)
        return destDir

if __name__ == '__main__':
    if len(sys.argv) != 6:
        print 'Parameters incorrect.'
        print 'python %s <dataDir> <mapDir> <slide> <ref> <seqType>' % sys.argv[0]
        sys.exit()

    dataDir = sys.argv[1]
    mapDir = sys.argv[2]
    slide = sys.argv[3]
    reference = sys.argv[4]
    seqType = sys.argv[5]

    simpleTask = SimpleTask(dataDir, mapDir, slide, reference, seqType)
    simpleTask.constructDir()

