import os, sys
import subprocess


class ControlPermission(object):

    def __init__(self, configFile, jsonFile):
        if not os.path.exists(configFile) or not os.path.exists(jsonFile):
            raise "No such file: %s %s" % (configFile, jsonFile)

        self.config = {}
        self.dirInfo = []

        self.readConfig(configFile)
        self.readJsonFile(jsonFile)

    def changePermission(self):
        if len(self.dirInfo) == 0:
            return

        dirList = []
        for v in self.dirInfo.itervalues():
            dirList.append(os.path.join(self.config['addr'], v[0], v[1], v[2]))

        filePermission = self.config['filePermission']
        directoryPermission = self.config['directoryPermission']
        for d in dirList:
            if not os.path.exists(d):
                continue
            subprocess.Popen('/usr/bin/find %s -type f -exec chmod %s {} \; && /usr/bin/find %s -type d -exec chmod %s {} \;' % (d, filePermission, d, directoryPermission), shell=True)

            print 'Change Permission for %s' % (d, )

        

    def readConfig(self, configFile):
        import ConfigParser
        config = ConfigParser.ConfigParser()
        config.read(configFile)
        section = 'default'

        for k in ['directoryPermission', 'filePermission', 'addr']:
            self.config[k] = config.get(section, k)
        
    def readJsonFile(self, jsonFile):
        import json
        with open(jsonFile, 'r') as fhIn:
            data = fhIn.read()
            self.dirInfo = json.loads(data)

if __name__ == "__main__":
    currentDir = os.path.dirname(sys.argv[0])
    configFile = os.path.join(currentDir, 'config', 'permissionControl.cfg')
    jsonFile = os.path.join(currentDir, 'config', 'permissionControl.json')

    cp = ControlPermission(configFile, jsonFile)
    cp.changePermission()
