#!/usr/bin/env python
import os, sys
from manageConfig import ManageConfig
from manageTask import ManageTask
import json
import glob
import gzip
from sendMail import SendMail
import shutil
import subprocess
import re
import datetime
import copy


##### Chang Log
'''
    1.1.0   2015.11.13  Change judge transferStatus from '==Failed' to '!=Normal'

    1.2.0   2016.01.26  Add the control of inlane

'''
inlane = 'Inlane'
inlane_barcode = '_2382_'
inlane_seqtype = 'Ecoli.fa'
upload_option = ['machine', 'slide', 'lane', 'reference', 'seqType', 'uploadTime', 'transferStatus']

class Daemon(object):
    ''' Search the info uploaded, and raise the task '''

    def __init__(self, configFile, taskFile):
        self.configFile = configFile
        self.config = {}
        self.readConfig()
        self.taskFile = taskFile
        self.cycleThreshold = 25

    def readConfig(self):
        import ConfigParser
        config = ConfigParser.ConfigParser()
        config.read(self.configFile)
        section = 'default'
        self.config['uploadDir'] = config.get(section, 'uploadDir')
        self.config['archivedDir'] = config.get(section, 'archivedDir')
        self.config['loadFailedDir'] = config.get(section, 'loadFailedDir')
        self.config['noMapDir'] = config.get(section, 'noMapDir')
        self.config['dataDir'] = config.get(section, 'dataDir')
        self.config['inlaneDir'] = config.get(section, 'inlaneDir')
        self.config['mapDir'] = config.get(section, 'mapDir')
        self.config['inlaneMapDir'] = config.get(section, 'inlaneMapDir')
        self.config['programDir'] = config.get(section, 'programDir')
        self.config['taskScript'] = config.get(section, 'taskScript')
        self.config['submitServer'] = config.get(section, 'submitServer')

        try:
            self.config['backupDir'] = config.get(section, 'backupDir')
        except:
            self.config['backupDir'] = None

    def processTask(self):
        mc = ManageConfig(self.taskFile, self.config['uploadDir'], self.config['archivedDir'])
        taskData = mc.readConfigFile()
        taskData.update(mc.searchInfo())

        doneList = []
        inlaneDict = {}
        # Daemon for task
        for taskFlag in taskData:   
            task = taskData[taskFlag]
            uploadDir = self.config['uploadDir']
            slide = task['slide']
            lane = task['lane']
            transferStatus = task['transferStatus']
            machine = task['machine']
            dataDir = self.config['dataDir']
            mapDir = self.config['mapDir']
            # If there is dataDir in json, then it is a inlane task in last cycle
            if 'dataDir' in task: 
                dataDir = task['dataDir']
                mapDir = task['mapDir']
            infoFile = os.path.join(uploadDir, '%s_%s.txt' % (slide, lane))

            if transferStatus != 'Normal':
                ### Upload faield, then send email to me.
                sendMail = SendMail(machine, slide, lane, error=transferStatus)
                sendMail.sendMail()
                if os.path.exists(infoFile):
                    if transferStatus == 'Failed':
                        shutil.copy(infoFile, self.config['loadFailedDir'])
                    elif transferStatus == 'NoMapping':
                        shutil.copy(infoFile, self.config['noMapDir'])
                    else:
                        shutil.copy(infoFile, self.config['loadFailedDir'])
                    os.remove(infoFile)

                doneList.append(taskFlag)
                continue

            rawDataDir = os.path.join(dataDir, machine, slide, lane)

            ### For a new task, prepare data for raise the task
            if task['taskStatus'] == 'prepare':
                ### Cycle is too short, then does't mapping
                if self.judgeCycle(rawDataDir) < self.cycleThreshold:
                    print "%s_%s_%s can't mapping, cycle is less than %s" % (machine, slide, lane, self.cycleThreshold)
                    if os.path.exists(infoFile):
                        shutil.copy(infoFile, self.config['noMapDir'])
                        os.remove(infoFile)

                    doneList.append(taskFlag)
                    continue
                ### If there exists special barcode, the fake a new dir and as it a new lane
                specialBarcode = self.searchSpecialBarcode(rawDataDir)
                if specialBarcode:
                    fakeDir = os.path.join(self.config['inlaneDir'], machine, slide, lane)
                    if not os.path.exists(fakeDir):
                        os.makedirs(fakeDir)
                    for f in specialBarcode: 
                        shutil.copy(f, fakeDir)
                        os.remove(f)
                    newData = copy.deepcopy(task)
                    newData['dataDir'] = self.config['inlaneDir']
                    newData['mapDir'] = self.config['inlaneMapDir']
                    newData['reference'] = inlane_seqtype
                    inlaneDict['%s_%s_%s' % (slide, lane, inlane)] = newData

                try:
                    destDir = self.constructDir(machine, slide, lane, task['reference'], task['seqType'], dataDir, mapDir)
                    print 'startNewLane:\t%s' % ('_'.join([machine, slide, lane]))

                    p = subprocess.check_output(r'cd %s && sh run.sh' % (destDir), shell=True)
                    taskID = self.getTaskID(p)
                    task['taskID'] = taskID
                    task['taskStatus'] = 'Running'
                except Exception as e:
                    sendMail = SendMail(machine, slide, lane, error="Can't submit the task")
                    sendMail.sendMail()
                    print "errorLane(can't submit the task):\t%s" % ('_'.join([machine, slide, lane]))
                    task['taskStatus'] = 'Error'
                    #raise e
            ### For Error status, skip it
            elif task['taskStatus'] == 'Error':
                continue
            ### For Done status, output the end time
            elif task['taskStatus'] == 'Done':
                doneList.append(taskFlag)
                currentTime = datetime.datetime.now()
                startTime = datetime.datetime.strptime(task["uploadTime"], "%Y-%m-%d_%H:%M:%S")
                delta = currentTime - startTime

                # Prepare attachment
                # attachment = []
                # tmp_att = []
                # summaryReport = os.path.join(dataDir, machine, slide, lane, '%s_%s.summaryReport.html' % (slide, lane))
                # heatmapReport = os.path.join(dataDir, machine, slide, lane, '%s_%s.heatmapReport.html' % (slide, lane))
                # if re.search(inlane, dataDir):
                #     newSummaryReport = os.path.join(dataDir, machine, slide, lane, '%s_%s_%s.summaryReport.html' % (slide, lane, inlane))
                #     newHeatmapReport = os.path.join(dataDir, machine, slide, lane, '%s_%s_%s.heatmapReport.html' % (slide, lane, inlane))
                #     shutil.copy(summaryReport, newSummaryReport)
                #     shutil.copy(heatmapReport, newHeatmapReport)
                #     tmp_att.append(newSummaryReport)
                #     tmp_att.append(newHeatmapReport)
                # else:
                #     tmp_att.append(summaryReport)
                #     tmp_att.append(heatmapReport)
                # for a in tmp_att:
                #     if not os.path.exists(a):
                #         continue
                #     attachment.append(a)
                # if len(attachment) == 0:
                #     attachment = None
                # else:
                #     attachment = "%s" % ' '.join(attachment)
                # sendMail = SendMail(machine, slide, lane, attachment)
                # sendMail.sendMail()

                print 'doneLane:\t%s' % ('_'.join([machine, slide, lane])), '.\tIt cost', delta
                continue
            ### For Running status, Judgement the task's newest status
            elif task['taskStatus'] == 'Running':
                taskID = task['taskID']
                mt = ManageTask(taskID)
                status = mt.getTaskStatus()
                # Task Done
                if status == 'done':
                    task['taskStatus'] = 'Done'
                    taskID = task['taskID']
                    mt = ManageTask(taskID)
                    mt.deleteTask()

                    # Prepare attachment
                    attachment = []
                    tmp_att = []
                    summaryReport = os.path.join(dataDir, machine, slide, lane, '%s_%s.summaryReport.html' % (slide, lane))
                    heatmapReport = os.path.join(dataDir, machine, slide, lane, '%s_%s.heatmapReport.html' % (slide, lane))
                    if re.search(inlane, dataDir):
                        newSummaryReport = os.path.join(dataDir, machine, slide, lane, '%s_%s_%s.summaryReport.html' % (slide, lane, inlane))
                        newHeatmapReport = os.path.join(dataDir, machine, slide, lane, '%s_%s_%s.heatmapReport.html' % (slide, lane, inlane))
                        shutil.copy(summaryReport, newSummaryReport)
                        shutil.copy(heatmapReport, newHeatmapReport)
                        tmp_att.append(newSummaryReport)
                        tmp_att.append(newHeatmapReport)
                    else:
                        tmp_att.append(summaryReport)
                        tmp_att.append(heatmapReport)
                    for a in tmp_att:
                        if not os.path.exists(a):
                            continue
                        attachment.append(a)
                    if len(attachment) == 0:
                        attachment = None
                    else:
                        # Add backup of the reports and so they can copy to mv easily
                        backupDir = self.config['backupDir']
                        if backupDir:
                            reportDir = os.path.join(backupDir, machine, slide, lane)
                            if not os.path.exists(reportDir): os.makedirs(reportDir)

                            for att in attachment:
                                shutil.copy(att, reportDir)

                        attachment = "%s" % ' '.join(attachment)
                    sendMail = SendMail(machine, slide, lane, attachment)
                    sendMail.sendMail()
                # Task Error
                elif re.search('E', status):
                    print 'errorLane:\t%s' % ('_'.join([machine, slide, lane]))
                    task['taskStatus'] = 'Error'
                    sendMail = SendMail(machine, slide, lane, error="Program Error")
                    sendMail.sendMail()
                # Task keep Running
                else:
                    continue

            ### Prepare Mapping
            if os.path.exists(infoFile):
                shutil.copy(infoFile, self.config['archivedDir'])
                os.remove(infoFile)

        # Filter the done task
        tmpData = {}
        for taskFlag in taskData:   
            if taskFlag not in doneList:
                tmpData[taskFlag] = taskData[taskFlag]
        taskData = tmpData
        # Add the inlane task
        taskData.update(inlaneDict)

        mc.writeConfigFile(taskData)

    def getTaskID(self, outString):
        return outString.strip().split('\n')[-1].split()[-1]

    def judgeCycle(self, fqAddr):
        fqList = glob.glob(os.path.join(fqAddr, '*.fq.gz'))
        if len(fqList) == 0:
            return 0
        
        fq = None
        for f in fqList:
            if os.path.getsize(f) < 100:
                continue
            else:
                fq = f
                break
        if fq == None:
            return 0

        with gzip.open(fq, 'r') as fhIn:
            sequence = fhIn.readline().strip()
            sequence = fhIn.readline().strip()
            return len(sequence)
        return 0

    def constructDir(self, machine, slide, lane, reference, seqType, rawDataDir, destDir):
        rawDataDir = os.path.join(rawDataDir, machine, slide, lane)
        destDir = os.path.join(destDir, machine, slide, lane)

        ### 0. Remove the original dir
        if os.path.exists(destDir):
            shutil.rmtree(destDir)
        ### 1. Directory
        if not os.path.exists(destDir):
            os.popen(r'mkdir -p %s' % destDir)
        fqDir = os.path.join(destDir, 'FASTQ')
        if not os.path.exists(fqDir):
            os.popen(r'mkdir -p %s' % fqDir)
        link = os.path.join(fqDir, '%s_%s' % (slide, lane))
        if not os.path.exists(link):
            os.popen(r'ln -s %s %s' % (rawDataDir, link ))

        ### 2. Json file
        jsonFile = os.path.join(destDir, 'input.json')
        template = os.path.join(os.path.dirname(sys.argv[0]), 'config', 'input.json')
        with open(template, 'r') as fhIn:
            d= fhIn.read()
            jsonData = json.loads(d)

        ref = 'Ecoli'
        if reference == 'Ecoli.fa':
            ref = 'Ecoli'
        elif reference == 'Human.fa':
            ref = 'hg19'
        else:
            ref = self.reference.split('.')[0]
        jsonData["fqmap"]["ref"] = ref
            
        if seqType == 'PE':
            jsonData["input"]["isSe"] = False
        else:
            jsonData["input"]["isSe"] = True
        jsonData["cpasstat"]["runFqstat"] = True

        fhOut = open(jsonFile, 'w')
        fhOut.write(json.dumps(jsonData, indent=4, sort_keys=True))

        ### 3. Copy run.sh
        shellFile = os.path.join(destDir, 'run.sh')
        with open(shellFile, 'w') as fhOut:
            taskName = machine+'_'+slide+'_'+lane+'_report'
            #newVersion = ['Zebra68', 'Zebra69', 'Zebra63', 'Zebra43', 'Zebra44', 'Zebra10']
            #if machine in newVersion:
                #self.config['programDir'] = '/prod/stor-01/opa/analyses/00.workflow/0.8.0/opt70max'
            resultString = 'setcgi %s\n%s create %s %s' % (self.config['programDir'], self.config['submitServer'], taskName, self.config['taskScript'])
            fhOut.write(resultString)
        return destDir

    def searchSpecialBarcode(self, dataDir):
        special = []
        barcodes = inlane_barcode.split(',')
        if re.search(inlane, dataDir):
            return special
        for file in glob.glob(os.path.join(dataDir, '*')):
            if not os.path.isfile(file): continue
            for barcode in barcodes:
                if re.search(barcode, os.path.basename(file)):
                    special.append(file)
                    break
        return special

if __name__ == '__main__':
    configDir = os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), 'config')
    configFile = os.path.join(configDir, 'common.cfg')
    taskFile = os.path.join(configDir, 'task.json')

    print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    daemon = Daemon(configFile, taskFile)
    daemon.processTask()

