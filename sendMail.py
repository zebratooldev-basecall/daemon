import os, sys
import string
import json
import re

class SendMail(object):
    ''' This class is used for send mail '''

    def sendMail(self):
        sendNameString = ''
        for s in self.sendList:
            sendNameString += '%s ' % (s)
        for s in self.ccList:
            sendNameString += '-c %s ' % (s)
        
        if self.attachment != None:
            os.popen(r'echo "%s" | %s -e "set content_type=text/html" -s %s  %s  -a %s ' % (self.html, self.mutt, self.subject, sendNameString, self.attachment))
        else:
            os.popen(r'echo "%s" | %s -e "set content_type=text/html" -s %s  %s ' % (self.html, self.mutt, self.subject, sendNameString))

        #print (r'echo "%s" | mutt -e "set content_type=text/html" -s %s  %s  -a %s ' % (self.html, self.subject, sendNameString, self.attachment))

    def prepareInfo(self):
        transformOption = {'zebra': self.zebra, 'slide': self.slide, 'lane': self.lane}

        with open(self.mailTmpl, 'r') as fhIn:
            mailTemplate = fhIn.read()
            newStr = string.Template(mailTemplate)
            newStr = newStr.safe_substitute(transformOption)
            self.html = newStr

        with open(self.mailList, 'r') as fhIn:
            allOwner = fhIn.read()
            allOwner = json.loads(allOwner)
            if self.zebra in allOwner:
                sendList = allOwner[self.zebra]
            else:
                # maybe it is the new add machine, so send me
                sendList = allOwner['Zebra00']
            ccList = allOwner['CC']

        self.ccList = []
        self.sendList = []
        for c in ccList:
            if re.search('@', c):
                self.ccList.append(c)
            else:
                self.ccList.append(c+'@genomics.cn')
        for c in sendList:
            if re.search('@', c):
                self.sendList.append(c)
            else:
                self.sendList.append(c+'@genomics.cn')  

        self.subject = '%s_%s_%s' % (self.zebra, self.slide, self.lane)


    def prepareError(self, error):
        transformOption = {'zebra': self.zebra, 'slide': self.slide, 'lane': self.lane}

        self.html = '<html><b>%s of %s_%s_%s.</b></html>' % (error, self.zebra, self.slide, self.lane)

        with open(self.mailList, 'r') as fhIn:
            allOwner = fhIn.read()
            allOwner = json.loads(allOwner)
            sendList = allOwner['EE']

        self.sendList = []
        self.ccList = []
        for c in sendList:
            self.sendList.append(c+'@genomics.cn')

        self.subject = 'Error_%s_%s_%s' % (self.zebra, self.slide, self.lane)


    def __init__(self, zebra, slide, lane, attachment=None, error=None, mailTmpl=None, mailList=None):
        self.zebra = zebra
        self.slide = slide
        self.lane = lane
        self.attachment = attachment
        self.mutt = '/home/zhaofuxiang/lib/mutt/bin/mutt'

        if mailTmpl == None:
            self.mailTmpl = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config', 'mailTemplate.html')
        else:
            self.mailTmpl = mailTmpl

        if mailList == None:
            self.mailList = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config', 'mailOwnerList.json')
        else:
            self.mailList = mailList

        if error == None:
            self.prepareInfo()
        else:
            self.prepareError(error)


if __name__ == '__main__':
    zebra = sys.argv[1]
    slide = sys.argv[2]
    lane = sys.argv[3]
    attachment = sys.argv[4]
    if len(sys.argv) == 5:
        error = None
    else:
        error = sys.argv[5]

    sendMail = SendMail(zebra, slide, lane, attachment, error)
    sendMail.sendMail()

