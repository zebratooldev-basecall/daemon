import os, sys
import subprocess

class ManageTask(object):
    def __init__(self, taskID='0'):
        self.taskID = taskID
        self.projectID = 'F13ZOOYJSY1380'
        self.create = 'QS -p %s -w ' % (self.projectID)
        self.delete = 'drat delete'
        self.query = 'drat list'

    def getTaskStatus(self):
        #status = subprocess.check_output("%s %s| grep '\-------------' -A 1|tail -1 |awk '{print $3}'" % (self.query, self.taskID), shell=True)
        status = subprocess.check_output("%s | grep ^%s |awk '{print $3}'" % (self.query, self.taskID), shell=True)
        return status.strip()

    def createTask(self, script, memory=10):
        status = subprocess.check_output("%s %s %s " % (self.create, memory, script), shell=True)
        taskID = int(status.strip().split()[0])
        self.taskID = taskID

        return self.taskID
        
    def setTaskID(self, taskID):
        self.taskID = taskID

    def deleteTask(self):
        #os.popen(r'%s %s' % (self.delete, self.taskID))
        subprocess.check_output(r'%s %s' % (self.delete, self.taskID), shell=True)

if __name__ == '__main__':
    mt = ManageTask(sys.argv[1])
    print mt.getTaskStatus()
    #mt.setTaskID(sys.argv[1])
    #mt.deleteTask()



